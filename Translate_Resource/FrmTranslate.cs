﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Translate_Resource
{
    public partial class FrmTranslate : Form
    {
        public FrmTranslate()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 此字段用于存储读取源文件的文本
        /// </summary>
        public string StrResource { get; set; }

        /// <summary>
        /// 此字段用于存储读取汉化参考文件的文本
        /// </summary>
        public string StrTranslate { get; set; }

        /// <summary>
        /// 源文件的后缀
        /// </summary>
        public string ExtensionResourse { get; set; }

        /// <summary>
        /// 汉化参考文件的后缀
        /// </summary>
        public string ExtensionTranslate { get; set; }

        private void btnResource_Click(object sender, EventArgs e)
        {
            this.StrResource = OpenFiles("请打开源文件", out string extension, out string box);
            txtResource.Text = box;
            this.ExtensionResourse = extension;
            int edit = 0;
            if (this.ExtensionTranslate != this.ExtensionResourse && !string.IsNullOrEmpty(this.ExtensionTranslate))
            {
                MessageBox.Show("源文件和汉化参考文件的后缀要求一致，请重试");
            }
            else if (txtResource.Text == txtTranslate.Text && txtTranslate.Text == "文件加载成功" && this.ExtensionResourse == this.ExtensionTranslate)
            {
                edit = BuildString(StrResource, StrTranslate, ExtensionResourse);
                MessageBox.Show(string.Format("总共替换了{0}条数据", edit));
            }
        }

        private void btnTranslate_Click(object sender, EventArgs e)
        {
            this.StrTranslate = OpenFiles("请打开汉化参考文件", out string extension, out string box);
            txtTranslate.Text = box;
            this.ExtensionTranslate = extension;
            int edit = 0;
            if (this.ExtensionTranslate != this.ExtensionResourse && !string.IsNullOrEmpty(this.ExtensionResourse))
            {
                MessageBox.Show("源文件和汉化参考文件的后缀要求一致，请重试");
            }
            else if (txtResource.Text == txtTranslate.Text && txtTranslate.Text == "文件加载成功" && this.ExtensionResourse == this.ExtensionTranslate)
            {
                edit = BuildString(StrResource, StrTranslate, ExtensionResourse);
                MessageBox.Show(string.Format("总共替换了{0}条数据", edit));
            }
        }

        private static int BuildString(string resource, string translate, string extension)
        {
            char separator = extension == ".lang" ? '=' : ':';
            StringBuilder sb = new StringBuilder();
            string[] strResourse = resource.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            string[] strTranslate = translate.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            int edit = 0;
            for (int i = 0; i < strResourse.Length; i++)
            {
                int indexResourse = strResourse[i].IndexOf(separator);
                if (strResourse[i].Contains("en_us"))
                {
                    sb.Append(strResourse[i].Replace("en_us", "zh_CN") + "\r\n");
                    continue;
                }
                else if (indexResourse < 0)
                {
                    sb.Append(strResourse[i] + "\r\n");
                    continue;
                }
                else //已经到了开始对字典(:或者=分割成为字典)进行处理的流程了
                {
                    bool b = true;
                    for (int j = 0; j < strTranslate.Length; j++)
                    {
                        int indexTranslate = strTranslate[j].IndexOf(separator);
                        if (indexTranslate < 0)
                        {
                            continue;
                        }
                        else
                        {
                            if (strTranslate[j].Substring(0, indexTranslate).Trim() == strResourse[i].Substring(0, indexResourse).Trim())
                            {
                                sb.Append(strResourse[i].Substring(0, indexResourse) + strTranslate[j].Substring(indexTranslate) + "\r\n");
                                edit++;
                                b = false;
                                break;
                            }
                        }
                    }
                    if (b)
                    {
                        sb.Append(strResourse[i] + "\r\n");
                    }

                }
            }
            using (StreamWriter sw = new StreamWriter(@"zh_cn" + extension))
            {
                sw.Write(sb.ToString().Trim());
            }
            return edit;
        }


        private static string OpenFiles(string title, out string extension, out string box)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = title;
            ofd.Filter = "语言文件|*.lang;*.json";
            DialogResult result = ofd.ShowDialog();
            string path = ofd.FileName;
            extension = Path.GetExtension(path);
            StringBuilder sb = new StringBuilder();
            if (result == DialogResult.OK)
            {
                using (StreamReader fsReader = new StreamReader(path, Encoding.UTF8))
                {
                    while (!fsReader.EndOfStream)
                    {
                        sb.Append(fsReader.ReadLine() + "\r\n");
                    }
                }
                box = "文件加载成功";
            }
            else
            {
                box = "文件加载失败，请重新尝试";
            }
            return sb.ToString().Trim();
        }

        private void txtResource_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length > 1)
            {
                MessageBox.Show(string.Format("不支持{0}个文件同时拖入", files.Length));
                return;
            }

            if (Path.GetExtension(files[0]) == ".lang" || Path.GetExtension(files[0]) == ".json")  //判断文件类型，只接受.lang和.json文件
            {
                StringBuilder sb = new StringBuilder();
                using (StreamReader fsReader = new StreamReader(files[0], Encoding.UTF8))
                {
                    while (!fsReader.EndOfStream)
                    {
                        sb.Append(fsReader.ReadLine() + "\r\n");
                    }
                }
                StrResource = sb.ToString().Trim();
                ExtensionResourse = Path.GetExtension(files[0]);
                txtResource.Text = "文件加载成功";

                if (this.ExtensionTranslate != this.ExtensionResourse && !string.IsNullOrEmpty(this.ExtensionTranslate))
                {
                    MessageBox.Show("源文件和汉化参考文件的后缀要求一致，请重试");
                }
                else if (txtResource.Text == txtTranslate.Text && txtTranslate.Text == "文件加载成功" && this.ExtensionResourse == this.ExtensionTranslate)
                {
                    int edit = BuildString(StrResource, StrTranslate, ExtensionResourse);
                    MessageBox.Show(string.Format("总共替换了{0}条数据", edit));
                }
            }
            else
            {
                MessageBox.Show("仅支持.lang和.json文件，请重新确认文件格式");
            }
        }

        private void txtResource_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Link;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void txtTranslate_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length > 1)
            {
                MessageBox.Show(string.Format("不支持{0}个文件同时拖入", files.Length));
                return;
            }

            if (Path.GetExtension(files[0]) == ".lang" || Path.GetExtension(files[0]) == ".json")  //判断文件类型，只接受.lang和.json文件
            {
                StringBuilder sb = new StringBuilder();
                using (StreamReader fsReader = new StreamReader(files[0], Encoding.UTF8))
                {
                    while (!fsReader.EndOfStream)
                    {
                        sb.Append(fsReader.ReadLine() + "\r\n");
                    }
                }
                StrTranslate = sb.ToString().Trim();
                ExtensionTranslate = Path.GetExtension(files[0]);
                txtTranslate.Text = "文件加载成功";

                if (this.ExtensionTranslate != this.ExtensionResourse && !string.IsNullOrEmpty(this.ExtensionResourse))
                {
                    MessageBox.Show("源文件和汉化参考文件的后缀要求一致，请重试");
                }
                else if (txtResource.Text == txtTranslate.Text && txtTranslate.Text == "文件加载成功" && this.ExtensionResourse == this.ExtensionTranslate)
                {
                    int edit = BuildString(StrResource, StrTranslate, ExtensionResourse);
                    MessageBox.Show(string.Format("总共替换了{0}条数据", edit));
                }
            }
            else
            {
                MessageBox.Show("仅支持.lang和.json文件，请重新确认文件格式");
            }
        }

        private void txtTranslate_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Link;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
    }
}
