﻿namespace Translate_Resource
{
    partial class FrmTranslate
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTranslate));
            this.txtResource = new System.Windows.Forms.TextBox();
            this.txtTranslate = new System.Windows.Forms.TextBox();
            this.btnResource = new System.Windows.Forms.Button();
            this.btnTranslate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtResource
            // 
            this.txtResource.AllowDrop = true;
            this.txtResource.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtResource.Location = new System.Drawing.Point(0, 0);
            this.txtResource.Multiline = true;
            this.txtResource.Name = "txtResource";
            this.txtResource.Size = new System.Drawing.Size(358, 450);
            this.txtResource.TabIndex = 0;
            this.txtResource.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtResource_DragDrop);
            this.txtResource.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtResource_DragEnter);
            // 
            // txtTranslate
            // 
            this.txtTranslate.AllowDrop = true;
            this.txtTranslate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTranslate.Location = new System.Drawing.Point(358, 0);
            this.txtTranslate.Multiline = true;
            this.txtTranslate.Name = "txtTranslate";
            this.txtTranslate.Size = new System.Drawing.Size(358, 450);
            this.txtTranslate.TabIndex = 1;
            this.txtTranslate.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtTranslate_DragDrop);
            this.txtTranslate.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtTranslate_DragEnter);
            // 
            // btnResource
            // 
            this.btnResource.Location = new System.Drawing.Point(89, 190);
            this.btnResource.Name = "btnResource";
            this.btnResource.Size = new System.Drawing.Size(177, 60);
            this.btnResource.TabIndex = 2;
            this.btnResource.Text = "打开源文件";
            this.btnResource.UseVisualStyleBackColor = true;
            this.btnResource.Click += new System.EventHandler(this.btnResource_Click);
            // 
            // btnTranslate
            // 
            this.btnTranslate.Location = new System.Drawing.Point(447, 190);
            this.btnTranslate.Name = "btnTranslate";
            this.btnTranslate.Size = new System.Drawing.Size(177, 60);
            this.btnTranslate.TabIndex = 3;
            this.btnTranslate.Text = "打开汉化参考";
            this.btnTranslate.UseVisualStyleBackColor = true;
            this.btnTranslate.Click += new System.EventHandler(this.btnTranslate_Click);
            // 
            // FrmTranslate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 450);
            this.Controls.Add(this.btnTranslate);
            this.Controls.Add(this.btnResource);
            this.Controls.Add(this.txtTranslate);
            this.Controls.Add(this.txtResource);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmTranslate";
            this.Text = "汉化";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtResource;
        private System.Windows.Forms.TextBox txtTranslate;
        private System.Windows.Forms.Button btnResource;
        private System.Windows.Forms.Button btnTranslate;
    }
}

